// 键盘快捷键 read
'use strict';

(function(){
  var handleShortcut = function (event) {
    var keyCode = event.keyCode; // 事件键盘编号

    // area: R, keyCode: 82
    // viewport: V, keyCode: 86
    // fullpage: H, keyCode: 72
    if ( event.ctrlKey && event.shiftKey && (keyCode === 82 || keyCode === 86 || keyCode === 72) ) {
      event.preventDefault(); // 阻止默认

      chrome.runtime.sendMessage({ // 捕获到快捷键
        'msg': 'capture_hotkey',
        'keyCode': keyCode
      });
    }
  };

  if ( document.body.hasAttribute('beesoft_collector_injected') ) return; // 插件已加载

  document.body.setAttribute('beesoft_collector_injected', true); // 设置插件已加载

  document.body.addEventListener('keydown', handleShortcut, false); // 绑定快捷键
}());
