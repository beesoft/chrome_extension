;(function(){
  'use strict';

  chrome.runtime.onConnect.addListener(function(port) {
    port.onMessage.addListener(function(request) {
      if (request.msg === 'is_page_capturable') { // 是否页面可截屏
        try {
          if (isPageCapturable()) {
            port.postMessage({msg: 'capturable'}); // 可
          } else {
            port.postMessage({msg: 'uncapturable'}); // 不可
          }
        } catch(e) {
          port.postMessage({msg: 'loading'}); // 加载中
        }
      } else if (request.msg === 'is_page_pinable') { // 是否页面可交集图片
        if (~document.documentElement.className.indexOf('beesoft-loaded')) { // 可采集
          port.postMessage({msg: 'pinable'});
        } else { // 不可采集页面
          port.postMessage({msg: 'unpinable'});
        }
      }
    });
  });
})();
