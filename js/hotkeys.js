'use strict';

var HotKey = (function(){
  return {
    setup: function() {
      if ( !this.get('area') ) this.set('area', 'R');
      if ( !this.get('viewport') ) this.set('viewport', 'V');
      if ( !this.get('fullpage') ) this.set('fullpage', 'H');
      if ( !this.get('screen') ) this.set('screen', 'P');

      if ( this.isEnabled() ) this.set('screen', '@');
    },

    set: function(type, value) { // 设置
      localStorage.setItem(type + '_capture_hotkey', value);
    },
    get: function(type) { // 获取
      return localStorage.getItem(type + '_capture_hotkey');
    },

    getCharCode: function(type) { // 第一个字符编码
      return this.get(type).charCodeAt(0);
    },

    enable: function() { // 启用快捷键
      localStorage.setItem('hotkey_enabled', true);
    },
    disable: function() { // 禁用快捷键
      localStorage.setItem('hotkey_enabled', false);
    },
    isEnabled: function() { // 是否启用
      return localStorage.getItem('hotkey_enabled') === 'true';
    }
  }
})();
