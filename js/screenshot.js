'use strict';

var screenshot = {
  tab           : 0,
  canvas        : document.createElement("canvas"),
  startX        : 0,
  startY        : 0,
  scrollX       : 0,
  scrollY       : 0,
  docHeight     : 0,
  docWidth      : 0,
  visibleWidth  : 0,
  visibleHeight : 0,
  scrollXCount  : 0,
  scrollYCount  : 0,
  scrollBarX    : 17,
  scrollBarY    : 17,
  captureStatus : true,

  handleHotKey: function(keyCode) {
    if ( HotKey.isEnabled() ) { // 启用快捷键
      switch ( keyCode ) { // 键盘编码
        case HotKey.getCharCode('area'):
          screenshot.showSelectionArea();
          break;
        case HotKey.getCharCode('viewport'):
          screenshot.captureViewport();
          break;
        case HotKey.getCharCode('fullpage'):
          screenshot.captureFullpage();
          break;
        case HotKey.getCharCode('screen'):
          screenshot.captureScreen();
          break;
        default:
          break;
      }
    }
  },
  addMessageListener: function() { // 消息接收器
    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
      var obj = request;
      switch ( obj.msg ) {
        case 'capture_hotkey': // 快捷键
          screenshot.handleHotKey(obj.keyCode); // 处理快捷键
          break;
        case 'capture_selected': // 获取选择
          screenshot.captureSelected();
          break;
        case 'capture_area': // 截选择区域
          screenshot.showSelectionArea();
          break;
        case 'capture_viewport': // 截可是区域
          screenshot.captureViewport();
          break;
        case 'capture_fullpage': // 截当前页面
          screenshot.captureFullpage();
          break;
        default:
          break;
      }
    });
  },

  sendMessage: function(message, callback) { // 发消息
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, message, callback);
    });
  },

  captureViewport: function() { // view
    screenshot.sendMessage({msg: 'capture_viewport'}, screenshot.onResponseVisibleSize);
  },
  captureFullpage: function() { // fullpage
    screenshot.sendMessage({msg: 'scroll_init'}, screenshot.onResponseVisibleSize);
  },
  showSelectionArea: function() { // Area
    screenshot.sendMessage({msg: 'show_selection_area'}, null);
  },
  captureSelected: function() { // - 选择
    screenshot.sendMessage({msg: 'capture_selected'}, screenshot.onResponseVisibleSize);
  },

  captureVisible: function(page_info) {
    var format = localStorage.screenshotFormat || 'png';

    chrome.tabs.captureVisibleTab(null, {format: format, quality: 100}, function(data) {
      var image = new Image();
      image.onload = function() {
        var width = image.width;
        var height = image.height;

        var canvasWidth = screenshot.canvas.width = screenshot.visibleWidth;
        var canvasHeight = screenshot.canvas.height = screenshot.visibleHeight;
        // 虚拟画布的宽高，供截图结果的计算使用
        var _canvasWidth = _.isRetinaDisplay() ? canvasWidth * window.devicePixelRatio : canvasWidth;
        var _canvasHeight = _.isRetinaDisplay() ? canvasHeight * window.devicePixelRatio : canvasHeight;
        screenshot.canvas.width = _canvasWidth;
        screenshot.canvas.height = _canvasHeight;

        var context = screenshot.canvas.getContext("2d");
        context.drawImage(image, 0, 0, _canvasWidth, _canvasHeight, 0, 0, _canvasWidth, _canvasHeight);
        screenshot.postImage(page_info);
      };
      image.src = data;
    });
  },
  captureVisibleSelected: function(page_info) {
    var format = localStorage.screenshotFormat || 'png';

    chrome.tabs.captureVisibleTab(null, {format: format, quality: 100}, function(data) {
      var image = new Image();
      image.onload = function() {
        var width = image.width;
        var height = image.height;

        var canvasWidth = screenshot.canvas.width;
        var canvasHeight = screenshot.canvas.height;
        // 虚拟画布的宽高，供截图结果的计算使用
        var _canvasWidth = _.isRetinaDisplay() ? canvasWidth * window.devicePixelRatio : canvasWidth;
        var _canvasHeight = _.isRetinaDisplay() ? canvasHeight * window.devicePixelRatio : canvasHeight;
        
        var startX = _.isRetinaDisplay() ? screenshot.startX * window.devicePixelRatio : screenshot.startX;
        var startY = _.isRetinaDisplay() ? screenshot.startY * window.devicePixelRatio : screenshot.startY;
        screenshot.canvas.width = _canvasWidth;
        screenshot.canvas.height = _canvasHeight;
        var context = screenshot.canvas.getContext("2d");
        context.drawImage(image, startX, startY, _canvasWidth, _canvasHeight, 0, 0, _canvasWidth, _canvasHeight);
        screenshot.postImage(page_info);
      };
      image.src = data;
    });
  },
  captureAndScroll: function(page_info) {
    var format = localStorage.screenshotFormat || 'png';

    chrome.tabs.captureVisibleTab(null, {format: format, quality: 100}, function(data) {
      var image = new Image();
      image.onload = function() {
        var context = screenshot.canvas.getContext('2d');

        // 实际画布的宽高
        var canvasWidth = screenshot.canvas.width;
        var canvasHeight = screenshot.canvas.height;

        // 虚拟画布的宽高，供截图结果的计算使用
        var _canvasWidth = _.isRetinaDisplay() ? canvasWidth * window.devicePixelRatio : canvasWidth;
        var _canvasHeight = _.isRetinaDisplay() ? canvasHeight * window.devicePixelRatio : canvasHeight;

        // 整个 document 的宽高
        var docWidth = _.isRetinaDisplay() ? screenshot.docWidth * window.devicePixelRatio : screenshot.docWidth;
        var docHeight = _.isRetinaDisplay() ? screenshot.docHeight * window.devicePixelRatio : screenshot.docHeight;

        // 在 X Y 轴中的滚动距离
        var scrollX = _.isRetinaDisplay() ? screenshot.scrollX * window.devicePixelRatio : screenshot.scrollX;
        var scrollY = _.isRetinaDisplay() ? screenshot.scrollY * window.devicePixelRatio : screenshot.scrollY;

        // 初始位置
        var startX = _.isRetinaDisplay() ? screenshot.startX * window.devicePixelRatio : screenshot.startX;
        var startY = _.isRetinaDisplay() ? screenshot.startY * window.devicePixelRatio : screenshot.startY;

        // 获取在 X Y 轴中滚动条的宽度，在这里，假定所有 OSX 系统下的滚动条都是隐藏的
        if (_.isThisPlatform('mac')) {
          screenshot.scrollBarX = screenshot.scrollBarY = 0;
        } else {
          screenshot.scrollBarY = screenshot.visibleHeight < screenshot.docHeight ? 17 : 0;
          screenshot.scrollBarX = screenshot.visibleWidth < screenshot.docWidth ? 17 : 0;
        }

        // 页面可视区域的宽高
        var visibleWidth = screenshot.visibleWidth < canvasWidth ? screenshot.visibleWidth : canvasWidth;
        var visibleHeight = screenshot.visibleHeight < canvasHeight ? screenshot.visibleHeight : canvasHeight;

        // 计算截图结果中可视区域的宽高，去掉滚动条
        var _visibleWidth = (image.width - screenshot.scrollBarY < _canvasWidth ? image.width - screenshot.scrollBarY : _canvasWidth);
        var _visibleHeight = (image.height - screenshot.scrollBarX < _canvasHeight ? image.height - screenshot.scrollBarX : _canvasHeight);

        // Get region capture start x coordinate.
        var zoom = screenshot.zoom;
        var x1 = startX - Math.round(scrollX * zoom);
        var y1 = startY - Math.round(scrollY * zoom);
        var x2 = 0;
        var y2 = 0;
        var w1 = 0;
        var h1 = 0;
        var w2 = 0;
        var h2 = 0;

        // 判断是否是最后一屏，如果是，需要计算出剩余宽高
        if ((screenshot.scrollYCount + 1) * _visibleWidth > _canvasWidth) {
          w1 = _canvasWidth % _visibleWidth;
          w2 = canvasWidth % visibleWidth;
          x1 = (screenshot.scrollYCount + 1) * _visibleWidth - _canvasWidth + startX - scrollX;
        } else {
          w1 = _visibleWidth;
          w2 = visibleWidth;
        }

        if ((screenshot.scrollXCount + 1) * _visibleHeight > _canvasHeight) {
          h1 = _canvasHeight % _visibleHeight;
          h2 = canvasHeight % visibleHeight;
          if ((screenshot.scrollXCount + 1) * _visibleHeight + scrollY < docHeight) {
            y1 = 0;
          } else {
            y1 = (screenshot.scrollXCount + 1) * _visibleHeight + scrollY - docHeight;
          }
        } else {
          h1 = _visibleHeight;
          h2 = visibleHeight;
        }
        x2 = screenshot.scrollYCount * visibleWidth;
        y2 = screenshot.scrollXCount * visibleHeight;
        context.drawImage(image, x1, y1, w1, h1, x2, y2, w2, h2);
        screenshot.sendMessage({
          msg: 'scroll_next',
          visibleWidth: visibleWidth,
          visibleHeight: visibleHeight
        }, screenshot.onResponseVisibleSize);
      };
      image.src = data;
    });
  },
  captureAndScrollDone: function(page_info) {
    screenshot.postImage(page_info);
  },
  postImage: function(page_info) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      screenshot.tab = tabs[0];
      screenshot.page_info = page_info;
    });
    chrome.tabs.create({'url': 'edit.html'});
  },
  onResponseVisibleSize: function(response) {
    switch(response.msg) {
      case 'capture_viewport':
        screenshot.visibleHeight = response.visibleHeight;
        screenshot.visibleWidth = response.visibleWidth;
        screenshot.captureVisible(response.page_info);
        break;
      case 'capture_viewport_selected':
        // 截图起始 x 坐标
        screenshot.startX = response.startX;
        // 截图起始 y 坐标
        screenshot.startY = response.startY;
        // 截图宽高
        screenshot.canvas.width = response.canvasWidth;
        screenshot.canvas.height = response.canvasHeight;
        screenshot.captureVisibleSelected(response.page_info);
        break;
      case 'scroll_init_done':
        screenshot.startX = response.startX;
        screenshot.startY = response.startY;
        screenshot.scrollX = response.scrollX;
        screenshot.scrollY = response.scrollY;
        screenshot.canvas.width = response.canvasWidth;
        screenshot.canvas.height = response.canvasHeight;
        screenshot.visibleHeight = response.visibleHeight;
        screenshot.visibleWidth = response.visibleWidth;
        screenshot.scrollXCount = response.scrollXCount;
        screenshot.scrollYCount = response.scrollYCount;
        screenshot.docWidth = response.docWidth;
        screenshot.docHeight = response.docHeight;
        screenshot.zoom = response.zoom;
        setTimeout(function() {
          screenshot.captureAndScroll(response.page_info);
        }, 100);
        break;
      case 'scroll_next_done':
        screenshot.scrollXCount = response.scrollXCount;
        screenshot.scrollYCount = response.scrollYCount;
        setTimeout(function() {
          screenshot.captureAndScroll(response.page_info);
        }, 100);
        break;
      case 'scroll_finished':
        screenshot.captureAndScrollDone(response.page_info);
        break;
      default:
        break;
    }
  },
  init: function() { // 初始化
    localStorage.screenshotFormat = localStorage.screenshotFormat || 'png';
    screenshot.addMessageListener();
  }
};
