;(function(){
  'use strict';

  document.documentElement.classList.add('beesoftChromeExtensionInstalled');

  chrome.storage.local.get('version', function(obj) {
    if ( obj.version ) document.documentElement.setAttribute('data-beesoft-chrome-extension-installed', obj.version);
  });
})();
