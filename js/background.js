'use strict';

function toggle(showButton) { // 切换按钮
  chrome.browserAction.setIcon({
    path: {
      '19': 'images/logo/logo_19' + (showButton ? '' : '_gray') + '.png',
      '38': 'images/logo/logo_38' + (showButton ? '' : '_gray') + '.png'
    }
  });
}
function showValidImages(data) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var tab = tabs[0];

    if ( tab.url.replace(/^https?:\/\/(www)?/, '').indexOf('beesoft.ink') === 0 ) return;

    if (data && data.mediaType === 'image' && data.srcUrl && !~data.srcUrl.indexOf('data:')) {
      var eImage = {
        src: data.srcUrl || '',
        url: data.pageUrl || '',
        img: {
          alt : '',
          src : data.srcUrl,
          width : 0,
          height : 0
        }
      };

      chrome.tabs.sendMessage(tab.id, { msg: 'pinImage', data: eImage }, function(response) {});
    } else {
      chrome.tabs.sendMessage(tab.id, { msg: 'showValidImages' }, function(response) {});
    }
  });
}
function dumpURL(data) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var tab = tabs[0];

    alert(tab.url)
  });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  switch(request.msg) {
    case 'bookmarklet':
      ajax({
        url: 'https://huaban.com/js/widgets.min.js?' + Math.floor(new Date()/1e7),
        success: function(script) {
          sendResponse({ code: script });
        }
      });

      return true;
    case 'toggle':
      var showButton = !!request.showButton;

      toggle(showButton);

      chrome.storage.local.set({ showButton: showButton });

      // Just send message, not need to sendResponse asynchronously
      chrome.tabs.query({ currentWindow: true }, function(tabs) {
        for (var i = 0, l = tabs.length; i < l; ++i) {
          chrome.tabs.sendMessage(tabs[i].id, { msg: 'toggle', showButton: showButton }, function() {});
        }
      });

      sendResponse({ showButton: showButton });
      break;
    case 'pinAll':
      showValidImages();
      break;
    case 'getImageData':
      var canvas = document.createElement('canvas');
      var imgSrcs = [];
      try {
        imgSrcs = JSON.parse(request.imgSrcs);
      } catch(e) {
        imgSrcs = null;
      };
      if (!imgSrcs) return sendResponse({ error: 'Image data parse error' });

      _.asyncMap(imgSrcs, function(src, done){

        var xhr = new XMLHttpRequest();
        xhr.open('GET', src, true);
        xhr.responseType = 'blob';

        xhr.onload = function(e) {
          if (this.status === 200) {
            var blob = this.response;
            var reader = new window.FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
              var base64data = reader.result;
              done(null, base64data);
            }
          }
        };

        xhr.send();

      }, function(err, results){
        if (err) return sendResponse({ error: err });

        var _images = JSON.stringify(results);
        sendResponse({ images: _images });
      });
      return true;
    default:
      break;
  }
});
var welcome = function() {
  var welcomeLink = 'https://www.beesoft.ink/about/';

  if (!chrome.notifications) {
    window.open(welcomeLink);
  } else {
    chrome.notifications.create('about', {
      type    : 'basic',
      iconUrl : '/images/logo/logo_48.png',
      title   : '感谢安装 BeeSoft.ink 采集插件',
      message : 'BeeSoft.ink 采集插件会自动在网页图片上添加一个「采集」按钮，还支持截图功能哦，想知道更多直接点击这里吧 :)'
    }, function(id) {
      chrome.notifications.onClicked.addListener(function(_id) {
        if ( _id === id ) window.open(welcomeLink);
      });
    });
  }
};
// 右键菜单
chrome.contextMenus.create({
  title : '采集图片/视频',
  contexts : [ 'all' ],
  documentUrlPatterns : [ 'http://*/*', 'https://*/*' ],
  onclick : showValidImages
});
chrome.contextMenus.create({
  title : '收藏网址',
  contexts : [ 'all' ],
  documentUrlPatterns : [ 'http://*/*', 'https://*/*' ],
  onclick : dumpURL
});
chrome.storage.local.get('minWidth', function(obj) { // 初始化设置最小宽度
  if (obj.minWidth) return;

  chrome.storage.local.set({ 'minWidth': 200 });
});
chrome.storage.local.get('showButton', function(obj) { // 初始化设置按钮图标
  var showButton = obj.showButton;
  if (showButton === undefined) {
    showButton = true;
    chrome.storage.local.set({ 'showButton': showButton });
  }

  toggle(showButton);
});
chrome.storage.local.get('notFirstRun', function(obj) { // 初始化第一次运行
  if (obj.notFirstRun) return;

  chrome.storage.local.set({ 'notFirstRun': true });
  welcome();
});

ajax({
  url: 'manifest.json',
  success: function(data) {
    chrome.storage.local.set({ 'version': data.version }, function() {});
  }
});

screenshot.init();

HotKey.setup();
