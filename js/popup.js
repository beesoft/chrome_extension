'use strict';

function $(id) { // 根据 ID 选择元素
  return document.getElementById(id);
}
function hasClass(element, className) { // 元素包含类
  return element.classList.contains(className);
}
function enableCapture() { // 启用页面截屏
  $('capture-area-item').classList.remove('disabled');
  $('capture-viewport-item').classList.remove('disabled');
  $('capture-fullpage-item').classList.remove('disabled');
}
function disableCapture() { // 禁用页面截屏
  $('capture-area-item').classList.add('disabled');
  $('capture-viewport-item').classList.add('disabled');
  $('capture-fullpage-item').classList.add('disabled');
}

function init() { // 初始化
  var links = document.getElementsByTagName('a');
  for (var i = 0; links[i]; i++) {
    links[i].addEventListener('click', function(){
      if (this.href && !~this.href.indexOf('chrome-extension')) window.open(this.href);
    });
  }

  var toggleBtn = $('toggle-btn'); // 显示采集按钮
  var toggleBtnText = toggleBtn.getElementsByClassName('title')[0];
  toggleBtn.addEventListener('click', function() {
    var showButton = hasClass(toggleBtnText, 'checked');
    chrome.runtime.sendMessage({ msg: 'toggle', showButton: !showButton }, function(response) {
      var op = response.showButton ? 'add' : 'remove';
      toggleBtnText.classList[op]('checked');
    });
  });
  chrome.storage.local.get('showButton', function(obj) {
    var op = obj.showButton ? 'add' : 'remove';
    toggleBtnText.classList[op]('checked');
  });

  var pinAllBtn = $('pin-all-btn');
  pinAllBtn.addEventListener('click', function() {
    if ( this.classList.contains('disabled') ) return;

    chrome.runtime.sendMessage({ msg: 'pinAll' }, function(response) {});
    setTimeout(function() { window.close(); }, 100);
  });

  var bg = chrome.extension.getBackgroundPage();

  var captureAreaItem = $('capture-area-item');
  var captureViewportItem = $('capture-viewport-item');
  var captureFullpageItem = $('capture-fullpage-item');

  captureAreaItem.classList.add('disabled'); // 初始禁用所有按钮
  captureViewportItem.classList.add('disabled');
  captureFullpageItem.classList.add('disabled');

  captureFullpageItem.addEventListener('click', function() {
    if ( this.classList.contains('disabled') ) return;

    bg.screenshot.captureFullpage();
    window.close();
  });
  captureViewportItem.addEventListener('click', function() {
    if ( this.classList.contains('disabled') ) return;

    bg.screenshot.captureViewport();
    window.close();
  });
  captureAreaItem.addEventListener('click', function() {
    if ( this.classList.contains('disabled') ) return;

    bg.screenshot.showSelectionArea();
    window.close();
  });

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var port = chrome.tabs.connect(tabs[0].id);
    port.onMessage.addListener(function(response) {
      if (response.msg === 'capturable') { // 启用页面截屏
        enableCapture();
      } else if (response.msg === 'uncapturable') { // 禁用页面截屏
        disableCapture();
      } else if (response.msg === 'loading') { // 加载中
      }

      if (response.msg === 'pinable') { // 采集图片/视频
        $('pin-all-btn').classList.remove('disabled');
      } else if (response.msg === 'unpinable') {
        $('pin-all-btn').classList.add('disabled');
      }
    });

    port.postMessage({msg: 'is_page_capturable'}); // 发送消息 js/isload.js
    port.postMessage({msg: 'is_page_pinable'});
  });
}

document.addEventListener('DOMContentLoaded', init); // 监听事件

chrome.runtime.onMessage.addListener(function(request, sender) {
  if (request.msg === 'page_capturable') { // 启用页面截屏
    enableCapture();
  } else if (request.msg === 'page_uncapturable') { // 禁用页面截屏
    disableCapture();
  }

  if (request.msg === 'pinable') { // 采集图片/视频
    $('pin-all-btn').classList.remove('disabled');
  } else if (request.msg === 'unpinable') {
    $('pin-all-btn').classList.add('disabled');
  }
});
